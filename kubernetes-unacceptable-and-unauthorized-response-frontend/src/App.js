import React from 'react';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {success: false};
  }

  async componentDidMount() {
    const response = await fetch("http://test.com/rest/control/ler", {
        method: "GET",
        headers: {
          "Accept": "text/plain",
          "Content-Type": "text/plain",
          "header1": "header1Value"
        }
      });
    this.setState({success: response.ok});
  }

  render() {
    return (<div>success in componentDidMount: {String(this.state.success)}</div>);
  }
}

export default App;
