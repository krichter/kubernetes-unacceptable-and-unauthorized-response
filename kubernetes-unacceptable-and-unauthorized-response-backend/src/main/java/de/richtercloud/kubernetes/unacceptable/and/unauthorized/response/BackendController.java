package de.richtercloud.kubernetes.unacceptable.and.unauthorized.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/control")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8081"})
public class BackendController {

    @GetMapping(path = "/ler", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> check(@RequestHeader("header1") final String header1) {
        return new ResponseEntity<>("request processed", HttpStatus.OK);
    }

    @PostMapping(path = "/ling", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> change(@RequestHeader("header1") final String header1) {
        return new ResponseEntity<>("request processed", HttpStatus.OK);
    }
}
