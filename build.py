#!/usr/bin/python3

import subprocess as sp
import os
import argparse


def build(mvn="mvn", yarn="yarn", docker="docker", kubectl="kubectl", namespace="test", push=False):
    backend_cwd = "kubernetes-unacceptable-and-unauthorized-response-backend"
    sp.check_call([mvn, "clean", "install"], cwd=backend_cwd)
    sp.check_call([docker, "build", "-t", "registry.gitlab.com/krichter/kubernetes-unacceptable-and-unauthorized-response-backend:stable", "--build-arg=JAR_FILE=target/kubernetes-unacceptable-and-unauthorized-response-backend-1.0-SNAPSHOT.jar", "."], cwd=backend_cwd)
    if push is True:
        sp.check_call([docker, "push", "registry.gitlab.com/krichter/kubernetes-unacceptable-and-unauthorized-response-backend:stable"])

    frontend_cwd = "kubernetes-unacceptable-and-unauthorized-response-frontend"
    sp.check_call([yarn, "build"], cwd=frontend_cwd)
    sp.check_call([docker, "build", "-t", "registry.gitlab.com/krichter/kubernetes-unacceptable-and-unauthorized-response-frontend:stable", "."], cwd=frontend_cwd)
    if push is True:
        sp.check_call([docker, "push", "registry.gitlab.com/krichter/kubernetes-unacceptable-and-unauthorized-response-frontend:stable"])

    operations_cwd = "kubernetes-unacceptable-and-unauthorized-response-operations"
    sp.check_call([kubectl, "delete", "namespace", namespace])
    sp.check_call([kubectl, "create", "namespace", namespace])
    ensure_basic_auth_secret_present(kubectl, namespace)
    sp.check_call([kubectl, "apply", "-f", "deployment.yml", "-n", namespace], cwd=operations_cwd)


def ensure_basic_auth_secret_present(kubectl, namespace):
    secret_name = "test-basic-auth"
    if os.path.exists("auth"):
        raise RuntimeError("remove file auth manually")
    try:
        sp.check_call(["htpasswd", "-b", "-c", "auth", "test", "test"])
        sp.check_call([kubectl, "create", "secret", "generic", secret_name, "--from-file=auth", "-n", namespace])
    finally:
        if os.path.exists("auth"):
            os.remove("auth")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--push", help="push docker images to registry.gitlab.com",
                        action="store_true")
    args = parser.parse_args()
    build(push=args.push)
